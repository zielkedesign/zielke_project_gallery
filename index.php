<?php
/*
Plugin Name: Zielke Design Project Gallery
Plugin URI: http://zielke.design/plugins/zielke-design-project-gallery
Description: A minimal design gallery for projects with multiple images.
Version: 2.1.1
Author: Terry Zielke
Author URI: http://zielke.design
License: GPLv2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: zpg
*/

/* -----------------------------------------------------------------
	
	CHANGE LOG FOR VERSION 2.1.1
	////////////////////////////
	
	1. increased security
	2. swapped old JS for reordering images with jQuert UI
	
----------------------------------------------------------------- */

/*
	ABORT
	
	If this file is called directly, abort.
*/
if ( ! defined( 'WPINC' ) ) { die; }

/*
	ACTIVATION
	
	Runs when plugin is activated.
*/
function zpg_activate_plugin() {
	// Do Nothing
}
register_activation_hook(__FILE__,'zpg_activate_plugin'); 

/*
	DEACTIVATE
	
	Runs when plugin is deactivated.
*/
function zpg_deactivate_plugin() {
	// Do Nothing
}
register_deactivation_hook( __FILE__, 'zpg_deactivate_plugin' );

function zpg_uninstall_plugin(){
	// Do Nothing
}
register_uninstall_hook(__FILE__, 'zpg_uninstall_plugin');


/*
	DASHBOARD PAGES
	
	Register dashboard pages for displaying
	a list of created galleries, and for
	settings that control how the galleries
	will appear.
*/
function zpg_register_dashboard_pages(){
	add_menu_page( 
		'Project Gallery',			// page title
		'Project Gallery',			// menu title
		'editor',					// capabilities
		'zpg-main',					// page slug
		'display_zpg_main_page',	// page content call
		'dashicons-format-gallery',
		11
	);
    add_submenu_page(
	    'zpg-main',					// parent slug
	    'Options',					// page title
	    'Options',					// menu title
	    'editor',					// required capabilities
	    'zpg-options',				// page slug
	    'display_zpg_options_page'	// callback function to display page content
    );
    
    /*
	    CALL SETTINGS
	    
	    Plugin settings function must
		be called from inside the page setup.
	*/
	add_action( 'admin_init', 'zpg_gallery_settings' );
}
add_action( 'admin_menu', 'zpg_register_dashboard_pages' );

function display_zpg_main_page(){
    include('admin/pages/main.php'); 
}
function display_zpg_options_page(){
    include('admin/pages/settings.php'); 
}

/*
	SETTINGS
	
	Here we register setting options
	that will controll what the gallery
	looks like.
*/
function zpg_gallery_settings() {
	// slideshow
	register_setting( 'zpg_settings_group', 'zpg_image_size' );
	register_setting( 'zpg_settings_group', 'zpg_full_screen' );
	register_setting( 'zpg_settings_group', 'zpg_animation' );
	register_setting( 'zpg_settings_group', 'zpg_description' );
	register_setting( 'zpg_settings_group', 'zpg_links' );
	register_setting( 'zpg_settings_group', 'zpg_pinterest' );
	// thumbnails
	register_setting( 'zpg_settings_group', 'zpg_constrain_width' );
	register_setting( 'zpg_settings_group', 'zpg_full_width' );
	register_setting( 'zpg_settings_group', 'zpg_show_name' );
	register_setting( 'zpg_settings_group', 'zpg_excerpt' );
}


/*
	CONENT TYPE
	
	Register a custom conent type called "Gallery",
	and apply labels.
*/
function zpg_gallery_post_int() {
	$labels = array(
		'name'                  => _x( 'Gallery', 'Post type general name', 'zielkeDesign' ),
		'singular_name'         => _x( 'Gallery', 'Post type singular name', 'zielkeDesign' ),
		'menu_name'             => _x( 'Gallery', 'Admin Menu text', 'zielkeDesign' ),
		'name_admin_bar'        => _x( 'Gallery', 'Add New on Toolbar', 'zielkeDesign' ),
		'add_new'               => __( 'Add New', 'zielkeDesign' ),
		'add_new_item'          => __( 'Add New Gallery', 'zielkeDesign' ),
		'new_item'              => __( 'New Gallery', 'zielkeDesign' ),
		'edit_item'             => __( 'Edit Gallery', 'zielkeDesign' ),
		'view_item'             => __( 'View Gallery', 'zielkeDesign' ),
		'all_items'             => __( 'All Galleries', 'zielkeDesign' ),
		'search_items'          => __( 'Search Galleries', 'zielkeDesign' ),
		'parent_item_colon'     => __( 'Parent Gallery:', 'zielkeDesign' ),
		'not_found'             => __( 'No galleries found.', 'zielkeDesign' ),
		'not_found_in_trash'    => __( 'No galleries found in Trash.', 'zielkeDesign' ),
		'featured_image'        => _x( 'Gallery Thumbnail', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'zielkeDesign' ),
		'set_featured_image'    => _x( 'Set thumbnail', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'zielkeDesign' ),
		'remove_featured_image' => _x( 'Remove thumbnail', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'zielkeDesign' ),
		'use_featured_image'    => _x( 'Use as thumbnail', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'zielkeDesign' ),
		'archives'              => _x( 'Gallery archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'zielkeDesign' ),
		'insert_into_item'      => _x( 'Insert into gallery', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'zielkeDesign' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this gallery', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'zielkeDesign' ),
		'filter_items_list'     => _x( 'Filter gallery list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'zielkeDesign' ),
		'items_list_navigation' => _x( 'Gallery list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'zielkeDesign' ),
		'items_list'            => _x( 'Gallery list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'zielkeDesign' ),
	);
 
	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => 'zpg-main',
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'gallery' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 1,
		'supports'           => array( 'title', 'editor', 'thumbnail' ),
	);
 
	register_post_type( 'gallery', $args );
}
add_action( 'init', 'zpg_gallery_post_int' );


/*
	SHORTCODE
	
	This shortcode displays a grid of all the
	galleries. Either the first image or a set
	cover image will be used as the thumbnail
	for each.
	
*/
require_once( plugin_dir_path( __FILE__ ) . 'shortcodes/gallery.php' );

/*
	TEMPLATE POST
	
	Create a post template to display a single
	gallery. 
*/
function zpg_single_gallery_template($single_template) {
     global $post;
     if ($post->post_type == 'gallery') {
          $single_template = dirname( __FILE__ ) . '/templates/single.php';
     }
     return $single_template;
}
add_filter( 'single_template', 'zpg_single_gallery_template' );

/*
	ENGUEUE SCRIPTS
	
	Frontend CSS and JS scripts for single
	and grid galleries.
*/
function zpg_frontend_scripts() {
	// CSS
	wp_enqueue_style( 'zpg_frontend_styles', plugin_dir_url( __FILE__ ) . 'css/zpg_frontend_styles.css');
	wp_enqueue_style( 'zpg_unslider_styles', plugin_dir_url( __FILE__ ) . 'includes/unslider/unslider.css');
	wp_enqueue_style( 'zpg_unslider_dots_styles', plugin_dir_url( __FILE__ ) . 'includes/unslider/unslider-dots.css');
    // JS
	wp_enqueue_script( 'zpg_frontend_scripts', plugin_dir_url( __FILE__ ) . 'js/zpg_frontend_scripts.js', array( 'jquery' ));
	wp_enqueue_script( 'zpg_unslider_scripts', plugin_dir_url( __FILE__ ) . 'includes/unslider/unslider-min.js', array( 'jquery' ));
}
add_action( 'wp_enqueue_scripts', 'zpg_frontend_scripts' );
/*
	Backend CSS and JS scripts
*/
function zpg_backend_scripts() {
	// CSS
	wp_enqueue_style( 'zpg_backend_styles', plugin_dir_url( __FILE__ ) . 'css/zpg_backend_styles.css');
	wp_enqueue_style( 'zpg_backend_styles', plugin_dir_url( __FILE__ ) . 'includes/jquery-ui/jquery-ui.css');
	// JS
	wp_enqueue_script( 'zpg_backend_scripts', plugin_dir_url( __FILE__ ) . 'js/zpg_backend_scripts.js', array( 'jquery', 'jquery-ui-sortable' ));
	wp_enqueue_script( 'zpg_wp_media_scripts', plugin_dir_url( __FILE__ ) . 'js/zpg_wp_media_scripts.js', array( 'jquery' ));
	
	//wp_enqueue_script( 'zpg_jquery_event_move', plugin_dir_url( __FILE__ ) . 'includes/jquery.event.move-master/js/jquery.event.move.js', array( 'jquery' ));
}
add_action( 'admin_enqueue_scripts', 'zpg_backend_scripts' );


/* -----------------------------------------------------------------
	Classes
----------------------------------------------------------------- */
// Set class files
require_once( plugin_dir_path( __FILE__ ) . 'admin/zpg_admin_class.php' );
// Begins execution of the class.

function run_zpg_admin_class() {

	$project = new zpg_post();		// Creates a project object
	$project->run();				// Runs initial method in object

}
run_zpg_admin_class();
/**/