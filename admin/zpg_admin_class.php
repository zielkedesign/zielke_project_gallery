<?	
/*
	START CLASS
*/
class zpg_post {
	
	var $name = 'gallery';

	// Run Class Methods
	public function run() {
		add_action( 'add_meta_boxes', array( $this, 'add_zpg_meta_boxes' ) );
		add_action( 'save_post', array( $this, 'save_post_meta_data' ) );
	}
	
	
	// Renders the meta box on the post
	public function add_zpg_meta_boxes() {
		add_meta_box(
			'zpg_gallery_meta',										// field id
			'Gallery Images',										// field diplayed title
			array( $this, 'display_gallery_meta_box_content'),		// field content
			'gallery',												// content type
			'normal'												// field location
		);
		add_meta_box(
			'zpg_link_meta',										// field id
			'Link',													// field diplayed title
			array( $this, 'display_link_meta_box_content'),			// field content
			'gallery',												// content type
			'normal'												// field location
		);
		add_meta_box(
			'zpg_order_meta',										// field id
			'Position',												// field diplayed title
			array( $this, 'display_order_meta_box_content'),		// field content
			'gallery',												// content type
			'side'													// field location
		);
	}
	
	
	// Content to display inside meta box
	public function display_gallery_meta_box_content( $post ) {
		include( 'views/zpg_images.php' );
	}
	public function display_link_meta_box_content( $post ) {
		include( 'views/zpg_link.php' );
	}
	public function display_order_meta_box_content( $post ) {
		include( 'views/zpg_order.php' );
	}
	
	
	// Save post meta data
	public function save_post_meta_data( $post_id ){
		// Check for varified nonce
		if ( ! isset( $_POST['zielke_project_gallery_nonce'] ) ||
			! wp_verify_nonce( $_POST['zielke_project_gallery_nonce'], 'zielke_project_gallery_nonce' ) ){
			return;
		}else{
			
			// Comma seperated list of image urls for gallery
			if ( isset( $_REQUEST['gallery_array'] ) ) {
				update_post_meta( $post_id, 'gallery_array', sanitize_text_field($_REQUEST['gallery_array']) );
			}
			// URL for off site link
			if ( isset( $_REQUEST['gallery_link'] ) ) {
				update_post_meta( $post_id, 'gallery_link', sanitize_text_field($_REQUEST['gallery_link']) );
			}
			// Text for gallery link button
			if ( isset( $_REQUEST['gallery_link_text'] ) ) {
				update_post_meta( $post_id, 'gallery_link_text', sanitize_text_field($_REQUEST['gallery_link_text']) );
			}
			// Position in gallery
			if ( isset( $_REQUEST['zpg_order_position'] ) ) {
				update_post_meta( $post_id, 'zpg_order_position', sanitize_text_field($_REQUEST['zpg_order_position']) );
			}
		}
	}
	
}
/*
	END CLASS
*/