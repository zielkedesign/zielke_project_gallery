<div class="zpg-settings-wrapper">
<h1>Zielke Design Project Gallery Options</h1>

<h2>Displaying Thumbnail Grid</h2>
<p>This plugin allows you to manage multiple galleries on your website. They are all organized by a numbering system and displayed in a grid which you can place wherever you like. To display the grid of thumbnails for all the galleries copy and past this shortcode at the desired location.</p>
<p><code>[zielke_project_gallery]</code></p>


<form method="post" action="options.php">
<?
settings_errors();
// Get Wordpress database
// global $wpdb;

/* --------------------------------------------------
	GET PLUGIN SETTINGS
-------------------------------------------------- */

// select settings group
settings_fields( 'zpg_settings_group' );
do_settings_sections( 'zpg_settings_group' );

// get current slideshow settings
$zpg_image_size			= esc_attr( get_option('zpg_image_size'));
$zpg_full_screen		= esc_attr( get_option('zpg_full_screen'));
$zpg_animation			= esc_attr( get_option('zpg_animation'));
$zpg_description		= esc_attr( get_option('zpg_description'));
$zpg_links				= esc_attr( get_option('zpg_links'));
$zpg_pinterest			= esc_attr( get_option('zpg_pinterest'));

// get current thumbnail settings
$zpg_constrain_width	= esc_attr( get_option('zpg_constrain_width'));
$zpg_full_width			= esc_attr( get_option('zpg_full_width'));
$zpg_show_name			= esc_attr( get_option('zpg_show_name'));
$zpg_excerpt			= esc_attr( get_option('zpg_excerpt'));
?>
	<h2>Gallery Slideshow Settings</h2>
	<table>
		<tr id="zpgImageSize">
			<th>
				Image Size:<br>
				<sub>(in percentage of window)</sub>
			</th>
			<td>
				<input type="number" name="zpg_image_size" id="zpg_image_size" value="<? if($zpg_image_size){ echo $zpg_image_size; }else{ echo '50'; } ?>" min="20" max="80"> <span>%</span>
			</td>
		</tr>
		<tr id="zpgFullScreenSlideshow">
			<th>Make Full Screen Slideshow: </th>
			<td><input type="checkbox" name="zpg_full_screen" id="zpg_full_screen" <? if($zpg_full_screen == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
		<tr id="zpgAnimation">
			<th>Transition Effect: </th>
			<td>
				<select name="zpg_animation" id="zpg_animation">
					
					<option value="horizontal" <? if($zpg_animation == 'horizontal'){ echo ' selected="selected"'; } ?>>horizontal</option>
					
					<option value="vertical" <? if($zpg_animation == 'vertical'){ echo ' selected="selected"'; } ?>>vertical</option>
					
					<option value="fade" <? if($zpg_animation == 'fade'){ echo ' selected="selected"'; } ?>>fade</option>
				</select>
			</td>
		</tr>
		<tr id="zpgDescription">
			<th>Display description below slideshow: </th>
			<td><input type="checkbox" name="zpg_description" id="zpg_description" <? if($zpg_description == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
		<tr id="zpgLinks">
			<th>Display Link Button:<br><sub>Only displays if field is filled</sub> </th>
			<td><input type="checkbox" name="zpg_links" id="zpg_links" <? if($zpg_links == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
		<tr id="zpgPinterest">
			<th>Show Pinterest Button: </th>
			<td><input type="checkbox" name="zpg_pinterest" id="zpg_pinterest" <? if($zpg_pinterest == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
	</table>
	
	<h2>Gallery Thumbnails Settings</h2>
	<table>
		<tr id="zpgConstrainWidth">
			<th>Thumbnail Grid Max-Width:<br><sub>in pixels</sub> </th>
			<td><input type="number" name="zpg_constrain_width" id="zpg_constrain_width" value="<? if($zpg_constrain_width){ echo $zpg_constrain_width; }else{ echo '1200'; } ?>" min="700" max="3000"></td>
		</tr>
		<tr id="zpgFullWidth">
			<th>Make Thumbnail Grid Full Width: </th>
			<td><input type="checkbox" name="zpg_full_width" id="zpg_full_width" <? if($zpg_full_width == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
		<tr id="zpgShowName">
			<th>Display Gallery Name Above Thumbnail: </th>
			<td><input type="checkbox" name="zpg_show_name" id="zpg_show_name" <? if($zpg_show_name == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
		<tr id="zpgExcerpt">
			<th>Display Description Excerpt: </th>
			<td><input type="checkbox" name="zpg_excerpt" id="zpg_excerpt" <? if($zpg_excerpt == 'on'){ echo 'checked="checked"'; }?>></td>
		</tr>
	</table>

	<? submit_button('Save Gallery Settings','primary','zpg_save_button'); ?>
</form>
</div>