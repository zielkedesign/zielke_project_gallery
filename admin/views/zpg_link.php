<?
	// Get current value for linked content
	$currentGalleryLink	= get_post_meta( $post->ID, 'gallery_link', true );
	$currentGalleryLinkText	= get_post_meta( $post->ID, 'gallery_link_text', true );
?>
<p>Enter a url starting with http:// to link this gallery to another page.</p>
<input type="text" id="gallery_link" name="gallery_link" value="<? echo $currentGalleryLink; ?>" style=" width:100%;" />

<p>Enter the text to display on the button.</p>
<input type="text" id="gallery_link_text" name="gallery_link_text" value="<? echo $currentGalleryLinkText; ?>" style=" width:100%;" />