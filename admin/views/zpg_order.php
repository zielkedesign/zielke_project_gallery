<?
	$zpgMetaOrder = get_post_meta( $post->ID, 'zpg_order_position', true );
?>
<label for="product_order_position">Ordering Position</label>
<input type="number" name="zpg_order_position" id="zpg_order_position" value="<?php if($zpgMetaOrder){echo $zpgMetaOrder;}else{echo '0';} ?>">
<br>
<sup>Higher numbers will be pushed down the page.</sup>
