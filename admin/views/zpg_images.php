<?
	// Set post nonce
	wp_nonce_field('zielke_project_gallery_nonce', 'zielke_project_gallery_nonce');
	// Get post meta data
	$currentGalleryArray	= get_post_meta( $post->ID, 'gallery_array', true );
?>
<link rel='stylesheet prefetch' href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css'>

<input type="hidden" id="gallery_array" name="gallery_array" value="<? echo $currentGalleryArray; ?>" style=" width:100%;" />

<?
	$intArray	= explode(',', $currentGalleryArray);
?>
<p>Drag and drop to reorder gallery images.</p>
<div id="zpg_userInterface">
	<ul id="zpg_ui_list">
		<?
			foreach($intArray as $item){
				if($item != ''){
					$itemURL = wp_get_attachment_image_url($item, 'thumbnail', 'false');
					echo '<li draggable="true"><figure><img src="'.$itemURL.'" alt="'.$item.'"></figure><a class="deleteImage">delete</a></li>';
				}
			}
		?>
	</ul>
</div>

<p>
	<a class="btn button primary" id="zpg_addAnotherImage">Add Another Image</a>
</p>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js'></script>