(function ($) {
	$(function(){
				
		/* ------------------------------------------------------------------------------
			Delete image.
		------------------------------------------------------------------------------ */
		$('.deleteImage').live('click',function(){
			var answer = confirm('Are you sure you want to delete this image?');
			
			if(answer){
				// get current csv value
				var galleryArray	= $('#gallery_array').val();
				
				// get value of selected image to delete
				var badImage		= $(this).parent().find('img').attr('alt');
				//alert(badImage);
				
				// remove selected image from csv array with trailing comma
				var newcsvOrder		= galleryArray.replace(badImage+',', '');
				// remove selected image from csv array with procedding comma
				var newcsvOrder		= newcsvOrder.replace(','+badImage, '');
				
				
				// update csv field
				$('#gallery_array').val(newcsvOrder);
				
				// delete list item from UI
				$(this).closest('li').remove();
			}
			else{
				// alert('you did no delete');
			}
		
		});
		
		/**/


	});
})(jQuery);