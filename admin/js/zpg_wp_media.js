
/* ---------------------------------------------------
	Callback function to display the media uploader
--------------------------------------------------- */

function zpgRenderMediaUploader( $ ) {
	'use strict';
	var file_frame, image_data, json;

	/**
	 * If an instance of file_frame already exists, then we can open it
	 * rather than creating a new instance.
	 */
	if ( undefined !== file_frame ) {

		file_frame.open();
		return;

	}

	/* ---------------------------------
		create new instance.
	--------------------------------- */
	file_frame = wp.media.frames.file_frame = wp.media({
		frame:    'post',
		state:    'insert',
		multiple: false
	});

	/* ------------------------------------------------
		Setup an event handler for what to do when 
		an image has been selected.
	------------------------------------------------ */
	file_frame.on( 'insert', function() {

		// Read the JSON data returned from the Media Uploader
		json = file_frame.state().get( 'selection' ).first().toJSON();

		// First, make sure that we have the URL of an image to display
		if ( 0 > $.trim( json.url.length ) ) {
			return;
		}

		// After that, set the properties of the image and display it
		$( '#ZMP_image_container' )
			.children( 'img' )
				.attr( 'src', json.url )
				.attr( 'alt', json.caption )
				.attr( 'title', json.title )
				.show()
			.parent()
			.removeClass( 'hidden' );
		
		// get media ID
		var newid			= json.id;
		
		// get media url
		var newurl			= json.url;
		// get gallery csv array
		var galleryArray	= $('#gallery_array').val();
		
		// append newurl to galleryArray
		var newcsv 			= galleryArray+','+newid;
		
		// split newcsv into array
		var newArray		= newcsv.split(',');
		
		$('#zpg_ui_list').append('<li draggable="true"><figure><img src="'+newurl+'" alt="'+newid+'"></figure><a class="deleteImage">delete</a></li>');
		
		zpgSetGallerySCV( $ );
		
	});

	// Now display the actual file_frame
	file_frame.open();

}

/* ------------------------------------------------------------------------------
	Set value for new gallery CSV field.
------------------------------------------------------------------------------ */
function zpgSetGallerySCV( $ ){
	
	// csv variable to collect gallery order
	var newcsvOrder = 'empty';
	
	$('#zpg_ui_list li figure img').each(function(){
		var thisValue = $(this).attr('alt');
		if(newcsvOrder == 'empty'){
			newcsvOrder = thisValue;
		}
		else{
			newcsvOrder = newcsvOrder +','+ thisValue;
		}
	});
	$('#gallery_array').val(newcsvOrder);
}

/* ------------------------------------------------------------------------------
	Click button event to open the Wordpress media window.
------------------------------------------------------------------------------ */
(function( $ ) {
	'use strict';

	$(function() {

	//	zpgRenderFeaturedImage( $ );

		$( '#zpg_addAnotherImage' ).on( 'click', function( evt ) {
			// Stop the anchor's default behavior
			evt.preventDefault();
			// Display the media uploader
			zpgRenderMediaUploader( $ );
		});

	});

})( jQuery );