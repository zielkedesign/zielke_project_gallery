(function ($) {
	$(function(){
		/* ------------------------------------------------------------------------------
			Delete image.
		------------------------------------------------------------------------------ */
		$('.deleteImage').live('click',function(){
			var answer = confirm('Are you sure you want to delete this image?');
			
			if(answer){
				// get current csv value
				var galleryArray	= $('#gallery_array').val();
				
				// get value of selected image to delete
				var badImage		= $(this).parent().find('img').attr('alt');
				//alert(badImage);
				
				// remove selected image from csv array with trailing comma
				var newcsvOrder		= galleryArray.replace(badImage+',', '');
				// remove selected image from csv array with procedding comma
				var newcsvOrder		= newcsvOrder.replace(','+badImage, '');
				
				
				// update csv field
				$('#gallery_array').val(newcsvOrder);
				
				// delete list item from UI
				$(this).closest('li').remove();
			}
			else{
				// alert('you did no delete');
			}
		
		});
		
		/**/
		
		/*
			SORTABLE
			
			First call functions from jQuery UI and then
			create a mouseup event to get the new order of images.
		*/
		$( function() {
			$( "#zpg_ui_list" ).sortable();
			$( "#zpg_ui_list" ).disableSelection();
		} );
		
		$('#zpg_ui_list').mouseup(function(){
			var imageOrder = 'empty';
			setTimeout(function(){
					
				$('#zpg_ui_list li figure img').each(function(){
					var mediaID = $(this).attr('alt');
					if(imageOrder == 'empty'){
						imageOrder = mediaID;
					}
					else{
						imageOrder = imageOrder +','+ mediaID;
					}
				});
				
				$('#gallery_array').val(imageOrder);

			}, 200);
			
		});

	});
})(jQuery);