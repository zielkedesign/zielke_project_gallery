(function ($) {
	$(function(){
		
		/* --------------------------------------------------------------------
			Unslider
		-------------------------------------------------------------------- */
		$('#vertical #zpg_unslider').unslider({
			autoplay: false,
			nav: false,
			arrows: true,
			infinite: false,
			animation: 'vertical',
			animateHeight: false,
			speed: 400
		});
		
		$('#horizontal #zpg_unslider').unslider({
			autoplay: false,
			nav: false,
			arrows: true,
			infinite: false,
			animation: 'horizontal',
			animateHeight: false,
			speed: 400
		});
		
		$('#fade #zpg_unslider').unslider({
			autoplay: false,
			nav: false,
			arrows: true,
			infinite: false,
			animation: 'fade',
			animateHeight: false,
			speed: 400
		});
		/**/
		/* --------------------------------------------------------------------
			Responsive nav buttons
		-------------------------------------------------------------------- */
		// Calculate height of buttons
		var zpg_img_height = $('.zpg_js_heigth').attr('id') - 10;
		// Set height of buttons
		$('#zpg_gallery .unslider-arrow').css('height', zpg_img_height+'vh');
		$('#zpg_gallery .unslider-arrow i').css('line-height', zpg_img_height+'vh');
		
	});
})(jQuery);