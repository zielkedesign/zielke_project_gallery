<?
	/* ------------------------------------------------------------------------------
		Get plugin global settings
	------------------------------------------------------------------------------ */
	$zpg_image_size			= esc_attr( get_option('zpg_image_size'));
	$zpg_full_screen		= esc_attr( get_option('zpg_full_screen'));
	$zpg_animation			= esc_attr( get_option('zpg_animation'));
	$zpg_description		= esc_attr( get_option('zpg_description'));
	$zpg_links				= esc_attr( get_option('zpg_links'));
	$zpg_pinterest			= esc_attr( get_option('zpg_pinterest'));
?>
<?php get_header(); ?>

<main id="<? echo $zpg_animation; ?>" class="<? if($zpg_full_screen == 'on'){ echo ' zpgFullscreenGallery '; } ?>">
	<!-- set values for JS interaction -->
	<!--<i id="<? echo $zpg_animation; ?>" class="zpg_js_animation"></i>-->
	<i id="<? echo $zpg_image_size; ?>" class="zpg_js_heigth"></i>
	
	
	<?php if (have_posts()): while (have_posts()) : the_post(); ?>
	<!-- Start gallery -->
	<section id="zpg_gallery">
			<?
				/* ------------------------------------------------------------------------------
					Get data for current gallery
				------------------------------------------------------------------------------ */
				
				// get gallery image CSV data
				$galleryCSV	= get_post_meta( get_the_ID(), 'gallery_array', true);
				
				// explode CSV into array
				$intArray	= explode(',', $galleryCSV);
				
				// get link, and link button text
				$galleryLink = get_post_meta( get_the_ID(), 'gallery_link', true);
				$galleryLinkText = get_post_meta( get_the_ID(), 'gallery_link_text', true);
			?>
			
			<div id="zpg_unslider">
				<ul <? if($zpg_full_screen != 'on'){ echo ' style="height:'.$zpg_image_size.'vh;"'; } ?>>
					<?
						if($zpg_full_screen == 'on'){
							foreach($intArray as $item){
								// Make sure that there is no coma space with no data
								if($item != ''){
									$imageURL = wp_get_attachment_image_url($item, 'full', 'false');
									echo '<li style="background-image:url('.$imageURL.');height:100vh;"><div class="flex"><figure></figure></div></li>';
								}
							}
						}
						else{
							foreach($intArray as $item){
								// Make sure that there is no coma space with no data
								if($item != ''){
									$imageURL = wp_get_attachment_image_url($item, 'full', 'false');
									echo '<li style="height:'.$zpg_image_size.'vh;"><div class="flex"><figure style="background-image:url('.$imageURL.');width:'.$zpg_image_size.'%;height:'.$zpg_image_size.'vh;"><img src="'.$imageURL.'" alt=""></figure></div></li>';
								}
							}
						}
						/**/
					?>
				</ul>
			</div><!-- / zpg_unslider -->
				
			<div class="zpg_content" <? if($zpg_full_screen == 'on' && get_the_content() != ''){ echo 'style="background:hsla(0,0%,100%,.9);z-index:200;"'; } ?>>
				<div id="mobileInstruction">
					<sup>Tap on left or right of the above image.</sup>
				</div>
				<?
					// If description option is checked
					if($zpg_description == 'on'): ?>
					<div class="information  <? if($zpg_full_screen == 'on'){ echo ' blackText '; } ?>">
						<?php the_content(); ?>
					</div>
				<? endif;?>
				
				<?
					
					// If pinterest is enabled
					if($zpg_pinterest == 'on'){
						echo '<a data-pin-custom="true" data-pin-do="buttonBookmark" href="https://www.pinterest.com/pin/create/button/" class="btn pin-btn"><i class="fa fa-pinterest" aria-hidden="true"></i> Pin It</a>';
					}
					
					// If lins are set to be visible
					if($zpg_links == 'on'){
						
						// If there is a link entered for this gallery
						if($galleryLink){ 
							echo '<a href="'.$galleryLink.'" target="_blank" rel="noopener noreferrer" class="btn button">'.$galleryLinkText.'</a>';
						}
					}
					// If linkes are enabled and there is a link
					if($zpg_links == 'on' && $galleryLink){
						echo '<a href="#zpg_albums" class="btn button primary"><i class="fa fa-chevron-down"></i></a>';
					}
					// Or if pinterest button is visible
					elseif($zpg_pinterest == 'on'){
						echo '<a href="#zpg_albums" class="btn button primary"><i class="fa fa-chevron-down"></i></a>';
					}
					// No other buttons are visible
					else{
						echo '<a href="#zpg_albums" class="anchorLink"><i class="fa fa-angle-down"></i></a>';
					}
				?>
			</div><!-- / zpg_content -->

	</section>
	<?php endwhile; endif; ?>
	
	<!-- Start Thumbnails -->
	<section id="zpg_albums">
		<?
			echo do_shortcode('[zielke_project_gallery]');
		?>
	</section>
</main>

<!-- Pinterest button script -->
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js" ></script>


<?php get_footer(); ?>